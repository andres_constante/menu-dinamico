@if ($item['submenu'] == [])
    <li> 
        <a href="{{ url($item['name']) }}">{{ $item['name'] }}</a>
    </li>
@else
    <li><!-- class="dropdown"-->
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $item['name'] }}<span class="caret"></span></a>
        @if($sub=="SI")             
                @if($four=='SI')
                    <ul class="nav nav-four-level collapse">
                @else
                    <ul class="nav nav-third-level collapse">
                @endif
        @else
            <ul class="nav nav-second-level collapse">
        @endif
            @foreach ($item['submenu'] as $submenu)
                @if ($submenu['submenu'] == [])
                    <li><a href="{{ url('menu',['id' => $submenu['id'], 'slug' => $submenu['slug']]) }}">{{ $submenu['name'] }} </a></li>
                @else                    
                    @include('partials.menu-item-ok', [ 'item' => $submenu,'sub'=>'SI','four'=>'SI'])
                @endif
            @endforeach
        </ul>
    </li>
@endif